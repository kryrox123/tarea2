function registrarUsuario() {
    const firstName = document.getElementById('first-name').value;
    const lastName = document.getElementById('last-name').value;
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    const confirmPassword = document.getElementById('confirm-password').value;

    if (password !== confirmPassword) {
        alert('Las contraseñas no coinciden. Por favor, inténtelo de nuevo.');
        return;
    }

    const user = { firstName, lastName, email };

    const users = JSON.parse(localStorage.getItem('users')) || [];
    users.push(user);

    localStorage.setItem('users', JSON.stringify(users));

    alert('Usuario registrado exitosamente');
    document.getElementById('first-name').value = '';
    document.getElementById('last-name').value = '';
    document.getElementById('email').value = '';
    document.getElementById('password').value = '';
    document.getElementById('confirm-password').value = '';
}

function mostrarUsuarios() {
    const users = JSON.parse(localStorage.getItem('users')) || [];

    if (users.length === 0) {
        alert('No hay usuarios registrados.');
        return;
    }

    const userList = document.createElement('ul');

    users.forEach(user => {
        const listItem = document.createElement('li');
        listItem.textContent = `Nombre: ${user.firstName}, Apellido: ${user.lastName}, Correo: ${user.email}`;
        userList.appendChild(listItem);
    });

    const newWindow = window.open('', '_blank');
    newWindow.document.write('<html><head><title>Usuarios Registrados</title></head><body>');
    newWindow.document.write('<h2>Usuarios Registrados</h2>');
    newWindow.document.write(userList.outerHTML);
    newWindow.document.write('</body></html>');
    newWindow.document.close();
}